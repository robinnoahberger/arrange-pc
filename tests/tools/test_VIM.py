import pytest

from arrange.tools.VIM import VIM


@pytest.fixture
def vim():
    tool = VIM("vim_test")
    tool.install()

    yield tool

    tool.delete()


def test_get_newest_version(vim, requests_mock):
    requests_mock.get(
        "https://api.github.com/repos/vim/vim-win32-installer/releases/latest",
        text='{"name": "v1.0.0"}'
    )
    assert vim.get_newest_version() == "1.0.0"

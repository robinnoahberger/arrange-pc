import pytest

from arrange.tools.Tool import Tool


@pytest.fixture
def small_tool():
    tool = Tool("Test")
    tool.prepare()
    yield tool
    tool.delete()


def test_get_installed_version(small_tool):
    assert small_tool.get_installed_version() == ""

import feedparser

from arrange.versioning.Versioning import Versioning


class WinSCPVersioning(Versioning):
    rss_feed_url = ""

    def __init__(self, rss_feed_url):
        self.rss_feed_url = rss_feed_url
        super(WinSCPVersioning, self).__init__()

    def detect_newest_version(self):
        rss_feed = feedparser.parse(self.rss_feed_url)
        for rss_entry in rss_feed.entries:
            title = rss_entry.title
            splitted_title = title.split(" ")
            if splitted_title[2] == 'released':
                newest_version = splitted_title[1]
                break
        return newest_version

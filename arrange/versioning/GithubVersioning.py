import requests

from arrange.versioning.Versioning import Versioning


class GithubVersioning(Versioning):
    github_project = ""
    github_subproject = ""

    github_version_url = ""

    def __init__(self, project, subproject):
        self.github_project = project
        self.github_subproject = subproject
        self.github_version_url = f"https://api.github.com/repos/{self.github_project}/" \
                                  f"{self.github_subproject}/releases/latest"
        super(GithubVersioning, self).__init__()

    def detect_newest_version(self):
        response = requests.get(self.github_version_url)
        newest_version = response.json()["name"][1:]
        return newest_version

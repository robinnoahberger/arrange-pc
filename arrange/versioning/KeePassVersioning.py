import feedparser

from arrange.versioning.Versioning import Versioning


class KeePassVersioning(Versioning):
    rss_feed_url = ""

    def __init__(self, rss_feed_url):
        self.rss_feed_url = rss_feed_url
        super(KeePassVersioning, self).__init__()

    def detect_newest_version(self):
        rss_feed = feedparser.parse(self.rss_feed_url)
        for rss_entry in rss_feed.entries:
            title = rss_entry.title
            newest_version = title.split(" ")[1]
            if newest_version.startswith("2"):
                break
        return newest_version

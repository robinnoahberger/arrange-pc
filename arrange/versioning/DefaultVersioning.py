from arrange.versioning.Versioning import Versioning


class DefaultVersioning(Versioning):

    def __init__(self, newest_version):
        self.newest_version = newest_version
        super(DefaultVersioning, self).__init__()

    def detect_newest_version(self):
        newest_version = self.newest_version
        return newest_version

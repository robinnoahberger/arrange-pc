import feedparser

from arrange.versioning.Versioning import Versioning


class RSSVersioning(Versioning):
    rss_feed_url = ""

    def __init__(self, rss_feed_url):
        self.rss_feed_url = rss_feed_url
        super(RSSVersioning, self).__init__()

    def detect_newest_version(self):
        rss_feed = feedparser.parse(self.rss_feed_url)
        newest_entry_title = rss_feed.entries[0].title
        newest_version = newest_entry_title.split(" ")[1]
        return newest_version

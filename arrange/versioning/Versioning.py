from abc import ABC


class Versioning(ABC):
    installed_version = ""
    newest_version = ""

    def __init__(self):
        self.installed_version = self.detect_installed_version()
        self.newest_version = self.detect_newest_version()

    # Need to Overwrite
    def detect_installed_version(self):
        pass

    def detect_newest_version(self):
        pass

    # Getter & Setter
    def get_installed_version(self):
        return self.installed_version

    def get_newest_version(self):
        return self.newest_version

    def set_newest_version(self, newest_version):
        self.newest_version = newest_version

    # Standard methods
    def persist_version_number(self, tool_directory):
        newest_version = self.get_newest_version()
        version_file = open(f"{tool_directory}version.txt", "w")
        version_file.write(newest_version)

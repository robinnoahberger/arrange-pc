import os

from arrange.tools.Tool import Tool
from arrange.versioning.KeePassVersioning import KeePassVersioning


class KeePass(Tool):
    versioning = KeePassVersioning("https://sourceforge.net/p/keepass/news/feed")

    def prepare(self):
        version = self.versioning.get_newest_version()
        self.download_url = f"https://sourceforge.net/projects/keepass/files/KeePass%202.x/{version}/KeePass-{version}.zip/download"
        os.mkdir(self.tool_directory)

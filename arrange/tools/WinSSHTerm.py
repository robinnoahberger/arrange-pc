import os
import shutil

from arrange.tools.Tool import Tool
from arrange.versioning.GithubVersioning import GithubVersioning


class WinSSHTerm(Tool):
    versioning = GithubVersioning("WinSSHTerm", "WinSSHTerm")

    def init(self):
        version = self.versioning.get_newest_version()
        version = version.split("-")[1]
        self.versioning.set_newest_version(version)

    def prepare(self):
        version = self.versioning.get_newest_version()
        self.download_url = f"https://github.com/WinSSHTerm/WinSSHTerm/releases/download/" \
                            f"{version}/WinSSHTerm-{version}.zip"
        os.mkdir(self.tool_directory)

    def unpack(self, tool_zip_file_path):
        shutil.unpack_archive(tool_zip_file_path, self.tool_directory)
        file_names = os.listdir(self.tool_directory + "WinSSHTerm")
        for file_name in file_names:
            shutil.move(self.tool_directory + "WinSSHTerm/" + file_name, self.tool_directory)

        os.remove(tool_zip_file_path)
        os.rmdir(self.tool_directory + "WinSSHTerm")

import os
import re
from urllib.request import urlopen
from bs4 import BeautifulSoup

from arrange.tools.Tool import Tool
from arrange.versioning.DefaultVersioning import DefaultVersioning


class Putty(Tool):
    def init(self):
        html_source = urlopen('https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html')
        parsed_html = BeautifulSoup(html_source.read(), 'html.parser')
        h1_string = parsed_html.h1
        array_touple_match = re.findall(r'(\d+\.)?(\d+\.)(\*|\d+)', str(h1_string))
        newest_version = ''.join(array_touple_match[0])
        self.versioning = DefaultVersioning(newest_version)

        self.tool_directory = self.root_directory + 'winsshterm/tools/'

    def prepare(self):
        version = self.versioning.get_newest_version()
        self.download_url = f"https://the.earth.li/~sgtatham/putty/{version}/w64/putty.zip"
        os.mkdir(self.tool_directory)
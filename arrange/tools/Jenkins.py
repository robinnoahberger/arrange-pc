import os
import zipfile
import feedparser

from arrange.tools.Tool import Tool
from arrange.versioning.RSSVersioning import RSSVersioning


class Jenkins(Tool):
    versioning = RSSVersioning("https://www.jenkins.io/changelog/rss.xml")
    file_archive_extension = "war"

    def prepare(self):
        version = self.versioning.get_newest_version()
        self.download_url = f"https://get.jenkins.io/war/{version}/jenkins.war"
        os.mkdir(self.tool_directory)

    # the war file is used in an unpacked state
    def unpack(self, tool_zip_file_path):
        pass

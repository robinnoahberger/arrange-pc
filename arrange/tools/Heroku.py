import os

from arrange.tools.Tool import Tool
from arrange.versioning.GithubVersioning import GithubVersioning


class Heroku(Tool):
    file_archive_extension = "tar.gz"
    versioning = GithubVersioning("heroku", "cli")

    def prepare(self):
        version = self.versioning.get_newest_version()
        self.download_url = f"https://github.com/heroku/cli/archive/refs/tags/v{version}.tar.gz"
        os.mkdir(self.tool_directory)

import os
import shutil
import requests

from abc import ABC


class Tool(ABC):
    name = None

    root_directory = "D:/apps/"
    tool_directory = ""
    download_url = ""

    latest_version = ""
    installed_version = ""

    file_archive_extension = "zip"
    versioning = ""

    def __init__(self, name):
        self.name = name
        self.tool_directory = self.root_directory + name.lower() + "/"
        self.init()

    def init(self):
        pass

    def is_installed(self):
        if os.path.isdir(self.tool_directory):
            return True
        return False

    def install(self):
        tool_zip_file_path = f"{self.tool_directory}{self.name.lower()}.{self.file_archive_extension}"
        self.prepare()
        self.download(tool_zip_file_path)
        self.unpack(tool_zip_file_path)
        self.versioning.persist_version_number(self.tool_directory)

    def update(self):
        self.delete()
        self.install()

    def prepare(self):
        os.mkdir(self.tool_directory)

    def download(self, tool_zip_file_path):
        download_request = requests.get(self.download_url, allow_redirects=True)

        tool_zip_file = open(tool_zip_file_path, "wb")
        tool_zip_file.write(download_request.content)
        tool_zip_file.close()

    def unpack(self, tool_zip_file_path):
        shutil.unpack_archive(tool_zip_file_path, self.tool_directory)
        os.remove(tool_zip_file_path)

    def persist_version_number(self):
        pass

    # returns a string of the newest version of the tool
    def get_newest_version(self):
        return self.versioning.get_newest_version()

    # function to delete all installed parts of the tool
    def delete(self):
        shutil.rmtree(self.tool_directory)

    # the following functions normally do not have to be overwritten
    # returns the installed version of the tool
    def get_installed_version(self):
        try:
            with open(self.tool_directory + "version.txt") as version_file:
                self.installed_version = version_file.read()
        except FileNotFoundError:
            pass
        return self.installed_version

    def is_update_available(self):
        is_update_available = False
        if self.get_installed_version() != self.get_newest_version():
            is_update_available = True
        return is_update_available

import os

from arrange.tools.Tool import Tool
from arrange.versioning.WinSCPVersioning import WinSCPVersioning


class WinSCP(Tool):
    versioning = WinSCPVersioning("https://winscp.net/feed.php")

    def init(self):
        self.tool_directory = self.root_directory + 'winsshterm/tools/WinSCP/'

    def prepare(self):
        version = self.versioning.get_newest_version()
        self.download_url = f"https://winscp.net/download/files/202203301241b80f59901f6edcbdb03eb588eeb2e0b2/WinSCP-{version}-Portable.zip"
        os.mkdir(self.tool_directory)

import os
import shutil

from arrange.tools.Tool import Tool
from arrange.versioning.GithubVersioning import GithubVersioning


class VIM(Tool):
    versioning = GithubVersioning("vim", "vim-win32-installer")

    def prepare(self):
        version = self.versioning.get_newest_version()
        self.download_url = \
            f"https://github.com/vim/vim-win32-installer/releases/download/" \
            f"v{version}/gvim_{version}_x64.zip"
        os.mkdir(self.tool_directory)

    def unpack(self, tool_zip_file_path):
        shutil.unpack_archive(tool_zip_file_path, self.tool_directory)

        source_dir = self.tool_directory + f"vim/vim82"
        target_dir = self.tool_directory
        file_names = os.listdir(source_dir)

        for file_name in file_names:
            shutil.move(os.path.join(source_dir, file_name), target_dir)

        os.remove(tool_zip_file_path)
        os.rmdir(self.tool_directory + "vim/vim82")
        os.rmdir(self.tool_directory + "vim")

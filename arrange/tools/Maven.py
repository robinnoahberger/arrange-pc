import os
import shutil

import requests

from arrange.tools.Tool import Tool
from arrange.versioning.DefaultVersioning import DefaultVersioning


class Maven(Tool):

    def init(self):
        version_tags_url = "https://api.github.com/repos/apache/maven/tags"
        response = requests.get(version_tags_url)
        newest_version = response.json()[1]["name"].split("-")[1]
        self.versioning = DefaultVersioning(newest_version)

    def prepare(self):
        version = self.versioning.get_newest_version()
        self.download_url = f"https://dlcdn.apache.org/maven/maven-3/{version}" \
                            f"/binaries/apache-maven-{version}-bin.zip"
        os.mkdir(self.tool_directory)

    def unpack(self, tool_zip_file_path):
        version = self.versioning.get_newest_version()
        shutil.unpack_archive(tool_zip_file_path, self.tool_directory)

        source_dir = self.tool_directory + f"apache-maven-{version}"
        target_dir = self.tool_directory
        file_names = os.listdir(source_dir)

        for file_name in file_names:
            shutil.move(os.path.join(source_dir, file_name), target_dir)

        os.remove(tool_zip_file_path)
        os.rmdir(self.tool_directory + f"apache-maven-{version}")
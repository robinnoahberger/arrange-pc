import os

import requests

from arrange.tools.Tool import Tool
from arrange.versioning.DefaultVersioning import DefaultVersioning


class AutoHotKey(Tool):

    def init(self):
        version_text_url = "https://www.autohotkey.com/download/1.1/version.txt"
        newest_version = requests.get(version_text_url).text
        self.versioning = DefaultVersioning(newest_version)

    def prepare(self):
        version = self.versioning.get_newest_version()
        self.download_url = f"https://www.autohotkey.com/download/1.1/AutoHotkey_{version}.zip"
        os.mkdir(self.tool_directory)
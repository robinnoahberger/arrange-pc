# Set up

To configure which tools should be installed, you have to place a file with the pattern `*arrange.yml` in your home
directory. This directory is determined by `os.path.expanduser('~')`. Each tool must be separated by a new mirror line:

```yml
- name: "AutoHotkey"
- name: "KeePass"
```

The names must match exactly the name of the file where the implementation is placed.


# Implement new tool

Create a new file in the directory `tools` with the pattern `Name.py` with the following content:

```python
from arrange.tools.Tool import Tool


class Name(Tool):
    download_url = "<URL of the zip-file>"
```

Default behaviours:

* if the tool directory (`D:/apps/<toolname>`) exists nothing happens
* the downloaded archive gets unpacked
* the downloaded archive gets removed


# What is implemented yet?

| Tool       | download           | update              | persist_version_number | get_newest_version |
|------------|--------------------|---------------------|------------------------|--------------------|
| AutoHotKey | :arrows_clockwise: | :arrows_clockwise:  | :heavy_check_mark:     | :heavy_check_mark: |
| Heroku     | :arrows_clockwise: | :arrows_clockwise:  | :heavy_check_mark:     | :heavy_check_mark: |
| Jenkins    | :arrows_clockwise: | :arrows_clockwise:  | :heavy_check_mark:     | :heavy_check_mark: |
| KeePass    | :arrows_clockwise: | :arrows_clockwise:  | :heavy_check_mark:     | :heavy_check_mark: |
| VIM        | :arrows_clockwise: | :arrows_clockwise:  | :heavy_check_mark:     | :heavy_check_mark: |



import glob
import importlib
import os
import socket
import yaml
import time


computer_name = socket.gethostname()

print(f"The arrangement of the computer {computer_name} will be started.")
print(f"Searching for files with pattern *arrange.yml in {os.path.expanduser('~')}")

summary_list = []
summary_list_header = [
    "Tool-Name",
    "Old Version",
    "New Version",
    "Time"
]
summary_list.append(summary_list_header)

for yaml_configuration_file in glob.glob(f"{os.path.expanduser('~')}/*arrange.yml"):
    with open(fr'{yaml_configuration_file}') as configuration_file:
        tool_configuration = yaml.load(configuration_file, Loader=yaml.FullLoader)

        for tool in tool_configuration:
            time_start = time.time()
            summary_tool_list = []
            tool_name = tool["name"]
            summary_tool_list.append(tool_name)
            tool_module = importlib.import_module(f'arrange.tools.{tool_name}')
            tool_object = getattr(tool_module, tool_name)(tool_name)

            if not tool_object.is_installed():
                print(f"{tool_name} will be installed. Installation starts now ...")
                tool_object.install()
                summary_tool_list.append("-")
                summary_tool_list.append(tool_object.get_newest_version())
                time_stop = time.time()
                time_total = time_stop - time_start
                summary_tool_list.append(f"{round(time_total, 2)} s")
                summary_list.append(summary_tool_list)
            else:
                summary_tool_list.append(tool_object.get_installed_version())
                if tool_object.is_update_available():
                    print(f"{tool_name} will be updated. Update starts now ...")
                    tool_object.update()
                    summary_tool_list.append(tool_object.get_newest_version())
                    time_stop = time.time()
                    time_total = time_stop - time_start
                    summary_tool_list.append(f"{round(time_total, 2)} s")
                    summary_list.append(summary_tool_list)


for tool in summary_list:
    print("{: <15} {: <15} {: <15} {: >8}".format(*tool))

